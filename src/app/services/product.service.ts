import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ProductModelServer } from '../models/product.model';
import { ServerResponse } from 'http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {


  private SERVER_URL = environment.SERVER_URL;
  constructor(private http: HttpClient) { }


  getAllProducts(numberOfResults = 10): Observable<ServerResponse>{
    return this.http.get<ServerResponse>(this.SERVER_URL + '/products', {
      params:{
        limit: numberOfResults.toString()
      }
    });
  }

  getSingleProduct(id: number): Observable<ProductModelServer>{
    return this.http.get<ProductModelServer>(this.SERVER_URL + '/products/' + id);
  }

  getProductsFromCategory(catName: string): Observable<ProductModelServer[]>{
    return this.http.get<ProductModelServer[]>(this.SERVER_URL+ '/products/category/' + catName);
    
  }
}